#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE gcdAlgTest
#include <boost/test/unit_test.hpp>
#include "euclidean_alg.h"
#include <stdexcept>


int x;
int y;


BOOST_AUTO_TEST_SUITE (Euclidean_Algorithm_Test_Suite)

//Standard number correctness unit test
BOOST_AUTO_TEST_CASE (correctness_test)
{
    BOOST_CHECK_EQUAL(gcdAlg(55,121,x,y), 11);
    BOOST_CHECK_EQUAL(gcdAlg(88,929838,x,y), 2);
    BOOST_CHECK_EQUAL(gcdAlg(56, 15,x,y), 1);
}

BOOST_AUTO_TEST_CASE (divide_by_zero)
{
    //BOOST_CHECK_THROW(gcdAlg(0,0,x,y), std::overflow_error);
}

//Negative Integers
BOOST_AUTO_TEST_CASE (negative_integers)
{
    BOOST_CHECK_EQUAL(gcdAlg(-6, -48,x,y), -6);
}

//Edge Cases
BOOST_AUTO_TEST_CASE (egde_case)
{
    //Smallest Non-Zero integer
    BOOST_CHECK_EQUAL(gcdAlg(1,1,x,y), 1);
    
    //Largest integer before conversion to long
    BOOST_CHECK_EQUAL(gcdAlg(19283729, 19839392,x,y), 1);
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE (Extended_Euclidean_Algorithm_Test_Suite)

BOOST_AUTO_TEST_CASE (correctness_test)
{
    gcdAlg(56,15,x,y);
    BOOST_CHECK_EQUAL(x,-4);
    BOOST_CHECK_EQUAL(y, 15);
    gcdAlg(88,21,x,y);
    BOOST_CHECK_EQUAL(x, -5);
    BOOST_CHECK_EQUAL(y, 21);
    
}

BOOST_AUTO_TEST_CASE (negative_integers)
{
    gcdAlg(-128, -8,x,y);
    BOOST_CHECK_EQUAL(x,1);
    BOOST_CHECK_EQUAL(y,0);
}

BOOST_AUTO_TEST_CASE (egde_case)
{
    //Smallest Non-Zero integer
    gcdAlg(1,1,x,y);
    BOOST_CHECK_EQUAL(x, 0);
    BOOST_CHECK_EQUAL(y, 1);
    
    gcdAlg(19283729,19839392,x,y);
    BOOST_CHECK_EQUAL(x, -8086865);
    BOOST_CHECK_EQUAL(y, 8319889);
}



BOOST_AUTO_TEST_SUITE_END()
