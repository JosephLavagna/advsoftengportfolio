#include <iostream>
#include <utility>
#include "euclidean_alg.h"

/* Implementation of Extended Euclidean Algorithm */

int gcdAlg( int a, int b, int& x, int& y)
{
  
    if (b > a)
    {
        std::swap(a, b);
    }
    int gcd;
    constexpr int maxCount = 100;
    int r;
    int count = 0;
    int q[maxCount];
    
    //While modulo of a and b is not equal to 0
    while ((r = a % b) !=0)
    {
        q[count++] = -(a/b);
        //var a is moved out of equation and b is moved into its position
        a = b;
        //as var b has been moved to a's position, it needs to be filled with the remainder
        b = r;
        //This loop is continued until it hits zero
    }
    
    //Extended part of algorithm.. to compute the solution to the gcd
    //x + y = gcd(a,b)
    if(count ==0)
    {
        
        x = 0;
        y = 1;
        gcd = b;
        return gcd;
    }
    else
    {
        x = 1;
        y = q[count-1];
        
        for (int i = count -2; i>=0; --i)
        {
            int t = x;
            x = y;
            y = t + y * q[i];
        }
    }
    gcd = b;
    return gcd;
    
    
}






